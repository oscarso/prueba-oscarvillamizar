import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './modules/users/users.module';
import { ConfigModule } from '@nestjs/config';
import { MailService } from './services-generals/mail/mail.service';
import { AccountsModule } from './modules/accounts/accounts.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import path, { join } from 'path';

@Module({
  imports: [

    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '../../src/', 'public'),
      exclude: ['/api*'],
    }),
    ConfigModule.forRoot({
      isGlobal: true, // no need to import into other modules
    }),
    MongooseModule.forRoot('mongodb://localhost/zuluPrueba'),
    MulterModule.registerAsync({
      useFactory: () => ({
        dest: './uploads',
      }),
    }),

    UsersModule,
    AccountsModule


  ],
  controllers: [AppController],
  providers: [AppService, MailService],
})
export class AppModule { }
