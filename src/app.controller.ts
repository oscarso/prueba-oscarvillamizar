import { Controller, Get, Res, HttpStatus, Param } from '@nestjs/common';
import { send } from 'process';
import { AppService } from './app.service';
const fs = require('fs');
const path = require('path');

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get()
  getHello(@Res() res) {
    this.appService.getHello().then((data: any)=>{
        res.sendFile(data);
    });
  }
  // get publications active
  @Get('/uploads/:name/:id')
  getCategories(@Res() res, @Param() params) {
    
    let pahtImg = path.resolve(__dirname, `../../uploads/${params.name}/${params.id}`);
    if( fs.existsSync(pahtImg)){
        res.sendFile(pahtImg);
    }else{
        let noImg = path.resolve(__dirname, '../../public/assets/logo.png');
        res.sendFile(noImg);
    }
    
    

  };
}
