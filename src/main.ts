
import { NestFactory } from '@nestjs/core';
import * as bodyParser from 'body-parser';

import { AppModule } from './app.module';
import {  port} from '../environment/enviroment';
import * as compression from 'compression';

import { DocumentBuilder, SwaggerModule} from "@nestjs/swagger"



async function bootstrap() {

  //const app = await NestFactory.create(AppModule,options);
    const app = await NestFactory.create(AppModule);
    const options = new DocumentBuilder()
    .setTitle("Pruebas-wallet")
    .setDescription("Api de pruebas")
    .setVersion("1.0")
    .addTag("pruebas-wallet")
    .build()

    const document = SwaggerModule.createDocument(app, options)
    SwaggerModule.setup("api/docs", app,document)
    // Initialize the firebase admin app
    app.enableCors();
    app.use(bodyParser.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
    app.enableCors();
    app.use(compression());
    await app.listen(port)
  
;
}
bootstrap();