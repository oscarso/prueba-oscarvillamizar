export const templatesendcode = `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Security-Policy" content="default-src https:">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pruebas</title>
    <style>
    .facet_sidebar {
        font-family: Monospace;
        color: white;
        margin-left: auto;
        margin-right: auto;
        width: 40%
    }

    .imgs {
        float: left;
        width: 50%;
    }

    .facet {
        float: left;
        width: 50%;
        text-align: center;
        padding-top: 10%;
    }

    @media (max-width: 600px) {
        .facet_sidebar {
            width: 100%;
            align-items: center;

        }

        .imgs {
            width: 100%;
            align-items: center;
        }

        .facet {
            width: 100%;
            align-items: center;
        }
    }
        </style>
</head>
<body style="background-color: black; width: 100%; overflow-x: hidden;">
  
    `

export const templebody = `
<div class="imgs">
<img
    src="https://www.gnoosis.com/____impro/1/onewebmedia/Captura%20de%20pantalla%202021-10-18%20a%20las%2022.49.58.png?etag=%2217cb3d-616dde1f%22&sourceContentType=image%2Fpng&ignoreAspectRatio&resize=473%2B625" />
</div>
</div>

</body>

</html>
`