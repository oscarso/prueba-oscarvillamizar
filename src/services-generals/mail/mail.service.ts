import { Injectable } from '@nestjs/common';
import { configEmail, url } from 'environment/enviroment';
import { templatesendcode, templebody } from './templates/codeRegistration';
const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  //service: 'smtp',
  host: configEmail.host,
  port: configEmail.port,
  secure: false,
  auth: {
    type: 'login',
    user: configEmail.emailSender,
    pass: configEmail.password
  }
});

@Injectable()
export class MailService {
  constructor() {}

  async sendUserConfirmation(user: any) {
    
    let mailOptions = {
      from: configEmail.emailSender,
      to: user.user.email,
      subject: 'Asunto Del Correo',
      html:`${templatesendcode}
      <div style="width: 100%; align-items: center;">
      <div class="facet">

          <h3 class="facet_sidebar">
              Welcome to Pruebas
          </h3>
          <p class="facet_sidebar">
              Thank you for being part of us, you just need to validate your email press the button to validate it
          </p>
          <a href=${url}verificate/${user.token} 
          style=" padding:2%; 
          margin-left: auto; margin-right: auto;
          background-color: #fff; color: #000; text-decoration:none
          ">
              Confirmar
          </a>
      

         
        
      </div>
      
      ${templebody}
      `
     
    };
  
     await transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error)
       return "error";
      } else {
       return 'Email enviado: ' + info.response;
      }
    });

  }

  async welcometoFrooget(user: any) {
    let mailOptions = {
      from: configEmail.emailSender,
      to: user.email,
      subject: 'Asunto Del Correo',
      html:`${templatesendcode}
      <p style="text-align: justify;">
        Your confirmation is complete. 
      </p>
      </div>
      </body>
      
      </html>`
     
    };
  
     await transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
       return error;
      } else {
       return 'Email enviado: ' + info.response;
      }
    });

  }

  async sendCodeRecuperte(user: any) {
    let mailOptions = {
      from: configEmail.emailSender,
      to: user.email,
      subject: 'Asunto Del Correo',
      html:`${templatesendcode}
      <p style="text-align: justify;">
        Your confirmation code is ${user.code}
      </p>
      </div>
      </body>
      
      </html>`
     
    };
  
     await transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error)
       return error;
      } else {
        console.log(info.response)
       return 'Email enviado: ' + info.response;
      }
    });

  }

  async yourPasswordRecuperate(user: any) {
    let mailOptions = {
      from: configEmail.emailSender,
      to: user.email,
      subject: 'Asunto Del Correo',
      html:`${templatesendcode}
      <p style="text-align: justify;">
        Your password is recuperate 
      </p>
      </div>
      </body>
      
      </html>`
     
    };
  
     await transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
       return error;
      } else {
       return 'Email enviado: ' + info.response;
      }
    });

  }
}



