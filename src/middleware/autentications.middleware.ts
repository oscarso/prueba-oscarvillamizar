import {  Injectable, NestMiddleware } from '@nestjs/common';
import { jwtConstants } from "../../environment/enviroment";

const jwt = require('jsonwebtoken')

@Injectable()
export class AutenticationsMiddleware implements NestMiddleware {
  constructor() {}
  use(req: Request, res: any, next: Function) {
    let token = req.headers['authorization'];
   
    jwt.verify(token , jwtConstants.secret, (err, decode) =>{
      if(err){
       
        return res.status(401).json({
          message : "No autorizado"
        })
      }
      
      res['user'] = decode
       next();
    })


  }
}
