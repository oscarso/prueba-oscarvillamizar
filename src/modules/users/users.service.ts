import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose'
import { InjectModel } from "@nestjs/mongoose"
import { Users } from './interfaces/users.interface';
import * as bcrypt from 'bcrypt';
import { jwtConstants } from "../../../environment/enviroment";
import { Accounts } from 'src/modules/accounts/interfaces/accounts.interface';


const jwt = require('jsonwebtoken')



@Injectable()
export class UsersService {

    constructor(@InjectModel('Accounts') private readonly accountsModel: Model<Accounts>,
        @InjectModel('Users') private readonly userModel: Model<Users>
    ) { }

    async login(body): Promise<any> {
      
        const user: any = await this.userModel.findOne(
            {
                $and:
                    [{ verificated: true },
                    {
                        $and: [
                            { enable: true },
                            { email: body.email  }
                        ]
                    }
                    ]
            })


        if (user) {

            let data = {
                fullname: user.fullname,
                direccional: user.direccional,
                cedula: user.cedula,
                email: user.email,
            }
            if (!bcrypt.compareSync(body.password, user.password)) {
                return "Invalid Credentials"

            }
            let message = "Welcome to Pruebas";
            let token = jwt.sign({
                data: user._doc
            }, jwtConstants.secret, { expiresIn: "24H" })
            return { data, token, message: message };

        }
        return "Invalid Credentials"


    }
 
    async getProfile(id: string): Promise<any> {

        const user: any = await this.userModel.findOne({ $and: [{ _id: id }, { enable: true }] }, { _id: false })

        if (user) {


            return user;
        }
        return "Invalid Credentials"


    }

    async createAccounts(body: any) {
        body.createdDate = Math.floor(Date.now() / 1000) + (60 * 60);
        body.updatedDate = Math.floor(Date.now() / 1000) + (60 * 60);
        body.deletedDate = Math.floor(Date.now() / 1000) + (60 * 60);
        const accounts = await new this.accountsModel(body)
        if (accounts) {
            await accounts.save()
        }
    }
    validarEmail(valor) {

        let re = /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/
        if (re.exec(valor)) {
            return true;
        } else {
            return false;
        }
    }
 
    async create(body: any) {
    
        if ( this.validarEmail(body.email)) {

            const valatido = await this.userModel.findOne({ email: body.email });

            if (!valatido) {

                const saltOrRounds = 10;
                const password = body.password || 'pruebas123**';
                body.password = await bcrypt.hash(password, saltOrRounds);
                const user: any = new this.userModel(body);
                if (user) {
                    this.createAccounts({
                        user_id: user.email,
                    }).then((data: any) => {
                    })
                    
                    let token = jwt.sign({
                        data: user._doc
                    }, jwtConstants.secret, { expiresIn: "480s" })
                    await user.save()
                    return { user, token }
                }
            }
            return "Error in supplied data"
        } else {
            return "You are not entering a valid email"
        }


    }

     async update(users: any, body: any) {
     
        const user = await this.userModel.findByIdAndUpdate(users, body, { new: true, runValidators: true })
        if (user) {
            return "Updated user"
        }
        return "User not found"
    }

    async delete(id) {
        const body: any = { enable: false }
        const user = await this.userModel.findByIdAndUpdate(id, body, { new: true, runValidators: true })
        if (user) {
            return "Usuario eliminado"
        }
        return "User not found"
    }

    async deleteFisical(id: string) {
        const user = await this.userModel.deleteOne({ _id: id });
        const accounts = await this.accountsModel.deleteOne({ users: id });
        if (user && accounts) {
            return "Usuario eliminado del sistema"
        }
        return "User not found"
    }

    async verificate(body) {
        const user: any = await this.userModel.findOne(
            {
                $and:
                    [{ verificated: false },
                    {
                        $and: [
                            { enable: true },
                            { email: body.email }
                        ]
                    }
                    ]
            })
        if (user) {
           
                let env: any = {
                    verificated: true,
                    updatedDate: Math.floor(Date.now() / 1000) + (60 * 60)
                }
                const veri = await this.userModel.findByIdAndUpdate(user._id, env, { new: true, runValidators: true })
                if (veri) {
                    return "Your account to verificated"
                }
              
            
        }
        return "Account is no verificable"
    }


}
