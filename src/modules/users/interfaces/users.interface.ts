import { Document } from 'mongoose'

export interface Users extends Document {
   readonly fullname: string;
   readonly direccional: string;
   readonly email: string; 
   readonly cedula: string;
   readonly password: string;
   readonly photoUrl: string; 
   readonly locations: string;
   readonly verificated: Boolean;
   readonly rol: string;

}