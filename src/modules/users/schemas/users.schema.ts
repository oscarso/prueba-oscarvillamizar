import { Schema } from 'mongoose';

let roleValiate = {
    values: [
    'ADMIN_ROLE', 
    'USER_ROLE',
    'JOURNALIST_ROLE',
    'PROFESSIONAL_ROLE',
    'EMPRESA_ROLE',
    'ONG_ROLE',
    'INFLUENCER_ROLE'    
],
    message: '{VALUE} no es un rol valido' 
};

export const userSchemas = new Schema({

    fullname:{ 
        type: String ,
        default: "users"
    },
 
    email: {
        type: String,
        required:  true,
        unique : true
    },
    direccional: {
        type: String, 
    },
    cedula:{
        type: String, 
    }, 
    password:{
        type: String,
        required:  true,
    },
    verificated: {
        type: Boolean,
        default: false
    },
    rol: {
        type: String,
        enum : roleValiate,
        default: "USER_ROLE"
    },
    createdDate:{
        type: Number
    },
    updatedDate:{
        type: Number
    },
    deletedDate:{
        type: Number,  
    },
    enable:{
        type: Boolean,
        default: true
    }

})
