import { Controller, Get, Post, Put, Delete, Res, HttpStatus, Body, Param, Req, UseInterceptors, UploadedFile, Query } from '@nestjs/common';
import { UsersService } from '../../users.service';


@Controller('users')
export class UsersController {

    constructor(private userService: UsersService) { }
    @Delete('/:id')
    deleteLogical(@Param() params, @Res() res) {
        this.userService.delete(params.id).then((data: any) => {
            res.status(HttpStatus.OK).json({
                message: 'Your account is delete',

            })
        }).catch((err: any) => {
            res.status(400).json({
                message: 'Your account not exists',

            })
        })
    }

    @Delete('/fisical/:id')
    deleteFisical(@Param() params, @Res() res) {
        this.userService.deleteFisical(params.id).then((data: any) => {
            res.status(HttpStatus.OK).json({
                message: 'Your account is delete',

            })
        }).catch((err: any) => {
            res.status(400).json({
                message: 'Your account not exists',

            })
        })
    }



}

@Controller('profile')
export class ProfileController {
    constructor(private userservice: UsersService) { }

    @Get('/:id')
    getUser(@Req() req) {
        let user = req.socket._httpMessage.req.res.user.data;
        return this.userservice.getProfile(user._id).then((data: any) => {
            return data
        }).catch(err => {
            return "Not aviable"
        })

    }

    @Put()
    async updateUser(@Res() res, @Body() body: any, @Req() req) {
        let user = req.socket._httpMessage.req.res.user.data;
        delete body.email;
        delete body.password;
        const update = await this.userservice.update(user._id, body)
        if (update) {
            res.status(HttpStatus.OK).json({
                ok: true,
                message: 'Your profile is update'
            })
        } else {
            res.status(400).json({
                ok: false,
                message: 'Your profile is not update'
            })
        }
    }

}

