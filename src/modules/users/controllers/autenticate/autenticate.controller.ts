import { Body, Controller, Get, HttpStatus, Param, Post, Req, Res, UploadedFile, UseInterceptors } from '@nestjs/common';
import { UsersService } from '../../users.service';
import { MailService } from 'src/services-generals/mail/mail.service';
import { jwtConstants } from "../../../../../environment/enviroment";


const jwt = require('jsonwebtoken')


@Controller('login')
export class AutenticateController {

    constructor(private UserService: UsersService) { }
    @Post('')
    async loginUser(@Body() body: any, @Res() res) {

        const datos = await this.UserService.login(body);

        if (datos === "Invalid Credentials") {
            res.status(400).json({
                ok: false,
                message: "Invalid Credentials"
            })
        }
        else {
            let { token, data, message } = datos;
            res.status(HttpStatus.OK).json({
                ok: true,
                token: token,
                message: message,
                data: data
            })
        }


    };

}

@Controller('verificate')
export class VerificateController {

    constructor(private UserService: UsersService, private readonly mail: MailService) { }

    @Get('/:id')
    verificateUserGet(@Res() res, @Param() params) {
        let text = "It has been successfully validated"
        let token = params.id

        jwt.verify(token, jwtConstants.secret, (err, decode) => {
            if (err) {

                return res.status(401).json({
                    message: "Not authorized"
                })
            }
            console.log(decode)
            let body = {
                email: decode.data.email,
                code: decode.data.code,
                token: decode.data.token
            }
            decode

            this.UserService.verificate(body).then((data: any) => {
                console.log(data);

                res.send(`
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Security-Policy" content="default-src https:">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pruebas</title>
    <style>
    .facet_sidebar {
        font-family: Monospace;
        color: white;
        margin-left: auto;
        margin-right: auto;
        width: 40%
    }

    .imgs {
        float: left;
        width: 50%;
    }

    .facet {
        float: left;
        width: 50%;
        text-align: center;
        padding-top: 10%;
    }

    @media (max-width: 600px) {
        .facet_sidebar {
            width: 100%;
            align-items: center;

        }

        .imgs {
            width: 100%;
            align-items: center;
        }

        .facet {
            width: 100%;
            align-items: center;
        }
    }
        </style>
</head>
<body style="background-color: black; width: 100%; overflow-x: hidden;">
                        <h1>Your Account is verificated</h1>
                 </body>
                `)
            }).catch(err => {
                return res.status(400).json({
                    ok: false,
                    message: "Account is not verificated"
                })
            })

        })




    }
}
@Controller('register')
export class RegisterController {

    constructor(private UserService: UsersService, private readonly mail: MailService) { }
    @Post('')
    async createUser(@Res() res, @Body() body: any) {

        let text = `Welcome to Gnoosis,  your code verications is: ${body.code}`

        const data = await this.UserService.create(body);

        if (data === "You are not entering a valid email" || data === "Error in supplied data") {

            return res.status(400).json({
                ok: false,
                message: data
            })
        } else {

            const email: any = await this.mail.sendUserConfirmation(data);



            res.status(HttpStatus.OK).json({
                ok: true,
                message: "Please confirm you mail, please check you spam folder",

            })

        }


    }
};