import { Test, TestingModule } from '@nestjs/testing';
import { AutenticateController } from './autenticate.controller';

describe('AutenticateController', () => {
  let controller: AutenticateController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AutenticateController],
    }).compile();

    controller = module.get<AutenticateController>(AutenticateController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
