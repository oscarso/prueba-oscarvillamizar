import { MiddlewareConsumer, Module } from '@nestjs/common';
import {  ProfileController, UsersController } from './controllers/users/users.controller';
import { UsersService } from './users.service';
import { AutenticateController, RegisterController, VerificateController } from './controllers/autenticate/autenticate.controller';
import { userSchemas } from './schemas/users.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from "../../../environment/enviroment";
import { AutenticationsMiddleware } from 'src/middleware/autentications.middleware';
import {  MailService } from 'src/services-generals/mail/mail.service';
import { accountsSchemas } from 'src/modules/accounts/schemas/accounts.schemas';



@Module({
  imports:[
    MongooseModule.forFeature([
      { name: 'Users', schema: userSchemas }, 
      { name: 'Accounts', schema: accountsSchemas},
    ]),
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '1000h' },
    }),
    
  ],
  controllers: [
    UsersController, 
    RegisterController, 
    AutenticateController, 
    ProfileController, 
    VerificateController
  ],
  providers: [UsersService,MailService]
})
export class UsersModule {

  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AutenticationsMiddleware)
      .forRoutes(ProfileController);
  }
}
