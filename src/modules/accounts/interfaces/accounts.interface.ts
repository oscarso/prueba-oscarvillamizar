import { Document } from 'mongoose';

export interface Accounts extends Document  {
    user_id: String,
    createdDate: Number,
    updatedDate: Number,
    deletedDate: Number,
    balanceCOP : Number,
    balanceUSD: Number
}
