import { Body, Controller, Get, Param, Put, Post, Res, Req, HttpStatus } from '@nestjs/common';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { AccountsService } from '../../accounts.service';

@Controller('getBalance')
export class AccountsController {

    constructor(private accountsService: AccountsService) { }

    @Get('')
    async getAccount(@Req() req, @Res() res) {
        let user = req.socket._httpMessage.req.res.user.data;

        const data: any = await this.accountsService.getAccount(user.email);

        res.status(HttpStatus.OK).json({
            ok: true,
            data
        })
    }

}

@Controller('tranfer')
export class TranferController {

    constructor(private accountsService: AccountsService) { }

    @Post()
    async operationsMoney(@Body() body: any,@Req() req, @Res() res) {
        let user = req.socket._httpMessage.req.res.user.data;
        const data: any = await this.accountsService.tranfer(body, user);
        if (data.status === true) {

            res.status(HttpStatus.OK).json({
                data
            })


        } else {

        }


    }

}

