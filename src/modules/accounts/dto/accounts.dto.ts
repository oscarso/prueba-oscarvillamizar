
export class Accounts  {
    user_id: String;
    createdDate: Number;
    updatedDate: Number;
    deletedDate: Number;
    balanceCOP: Number;
    balanceUSD: Number;
}