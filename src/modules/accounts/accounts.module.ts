import { MongooseModule } from '@nestjs/mongoose';

import { userSchemas } from '../users/schemas/users.schema';
import { AccountsService } from './accounts.service';
import { AccountsController, TranferController } from './controller/accounts/accounts.controller';
import { accountsSchemas } from './schemas/accounts.schemas';
import { MiddlewareConsumer, Module } from '@nestjs/common';
import { AutenticationsMiddleware } from 'src/middleware/autentications.middleware';
;
@Module({
  imports:[
    MongooseModule.forFeature([{ name: 'Users', schema: userSchemas }, {name: 'Accounts', schema: accountsSchemas}]),
    
  ],
  providers: [AccountsService],
  controllers: [AccountsController,TranferController]
})
export class AccountsModule {


  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AutenticationsMiddleware)
      .forRoutes(AccountsController,TranferController);
  }
}
