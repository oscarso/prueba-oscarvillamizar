import { Schema } from 'mongoose';

let roleValiate = {
    values: ['ADMIN_ROLE', 'USER_ROLE','JOURNALIST_ROLE','PROFESSIONAL_ROLE','EMPRESA_ROLE'],
    message: '{VALUE} no es un rol valido' 
};

export const accountsSchemas = new Schema({
    user_id: { 
        type: String ,
        required: true,
    },
    createdDate:{
        type: Number,  
    },
    updatedDate:{
        type: Number,    
    },
    deletedDate:{
        type: Number,
    },
    balanceUSD:{
        type: Number,
        default : 0
    },
    balanceCOP:{
        type: Number,
        default : 0
    }
   

})
