import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Users } from '../users/interfaces/users.interface';
import { Accounts } from './interfaces/accounts.interface';
import TrmApi from "trm-api";


const trm = new TrmApi("AkogDeC4OuCe2ApsEXLg9VCaL");

@Injectable()
export class AccountsService {

    constructor(@InjectModel('Accounts') private readonly accountsModel: Model<Accounts>,
        @InjectModel('Users') private readonly usersModel: Model<Users>,

    ) { }

    async tranfer(body: any, user: any) {
        const cambio: any = await trm.latest()
       

        const senderAccount: any = await this.accountsModel.findOne({ "user_id": user.email });

        if (body.type === "upload") {
            let env = {
                balanceCOP: 0,
                balanceUSD: 0,
                updatedDate: Math.floor(Date.now() / 1000) + (60 * 60)
            }
            if (body.currency === "COP") {
                env.balanceCOP = senderAccount.balanceCOP + body.money;
                env.balanceUSD = senderAccount.balanceUSD + (body.money / (parseFloat(cambio.valor)));
                const save = await this.accountsModel.findByIdAndUpdate(senderAccount._id,env)
                if (save) {
                    return { status: true, message: "Your mony is update" }
                }

            } else if (body.currency === "USD") {
                env.balanceCOP = senderAccount.balanceCOP + (parseFloat(cambio.valor) * body.money);
                env.balanceUSD = senderAccount.balanceUSD + body.money;
                const save = await this.accountsModel.findByIdAndUpdate(senderAccount._id,env)
                if (save) {
                    return { status: true, message: "Your mony is update" }
                }
            } else {
                return { status: false, message: "Currency not valid" }
            }
        } else if (body.type === "tranfer") {

            const receiberAccount: any = await this.accountsModel.findOne({ "user_id": user.email });
            let env = {
                balanceCOP: 0,
                balanceUSD: 0,
                updatedDate: Math.floor(Date.now() / 1000) + (60 * 60)
            }
            let envTwo = {
                balanceCOP: 0,
                balanceUSD: 0,
                updatedDate: Math.floor(Date.now() / 1000) + (60 * 60)
            }
            if (body.currency === "COP") {
                if (body.money <= senderAccount.balanceCOP) {
                    env.balanceCOP = receiberAccount.balanceCOP + body.money;
                    env.balanceUSD = receiberAccount.balanceUSD + (body.money / (parseFloat(cambio.valor)));
                    const save = await this.accountsModel.findByIdAndUpdate(receiberAccount._id,  env )
                    envTwo.balanceCOP = senderAccount.balanceCOP - body.money;
                    envTwo.balanceUSD = senderAccount.balanceUSD - (body.money / (parseFloat(cambio.valor)));
                    const saveTwo = await this.accountsModel.findByIdAndUpdate(senderAccount._id,envTwo )
                    if (save && saveTwo) {
                        return { status: true, message: "Your mony is update" }
                    }
                }


            } else if (body.currency === "USD") {
                if (body.money <= senderAccount.balanceUSD) {
                    env.balanceCOP = receiberAccount.balanceCOP + (parseFloat(cambio.valor) * body.money);
                    env.balanceUSD = receiberAccount.balanceUSD + body.money;
                    const save = await this.accountsModel.findByIdAndUpdate(receiberAccount._id,  env )
                    envTwo.balanceCOP = senderAccount.balanceCOP - (parseFloat(cambio.valor) * body.money);
                    envTwo.balanceUSD = senderAccount.balanceUSD - body.money;
                    const saveTWo = await this.accountsModel.findByIdAndUpdate(senderAccount._id,envTwo )
                    if (save) {
                        return { status: true, message: "Your mony is update" }
                    }
                }
            } else {
                return { status: false, message: "Currency not valid" }
            }

        } else {
            return { status: false, message: "No valid Operations" }
        }


    }

    async getAccount(id: string) {

        const account = await this.accountsModel.findOne({ user_id: id })

        return account

    }


}
